-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.6.38 - MySQL Community Server (GPL)
-- SE du serveur:                osx10.9
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de données de la table airbnb.equipements : ~6 rows (environ)
/*!40000 ALTER TABLE `equipements` DISABLE KEYS */;
INSERT INTO `equipements` (`id`, `label`) VALUES
	(1, 'Television'),
	(2, 'Toaster'),
	(3, 'Tumble drier'),
	(4, 'Washing machine'),
	(5, 'Heating'),
	(6, 'Elevator'),
	(7, 'Air-conditioning');
/*!40000 ALTER TABLE `equipements` ENABLE KEYS */;

-- Export de données de la table airbnb.equipement_room : ~11 rows (environ)
/*!40000 ALTER TABLE `equipement_room` DISABLE KEYS */;
INSERT INTO `equipement_room` (`room_id`, `equipement_id`) VALUES
	(84, 1),
	(84, 4),
	(84, 5),
	(85, 3),
	(85, 4),
	(85, 6),
	(86, 1),
	(86, 3),
	(86, 4),
	(86, 5);
/*!40000 ALTER TABLE `equipement_room` ENABLE KEYS */;

-- Export de données de la table airbnb.reservation : ~0 rows (environ)
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;

-- Export de données de la table airbnb.roles : ~2 rows (environ)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `label`) VALUES
	(1, 'ADVERTISER'),
	(2, 'STANDARD');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Export de données de la table airbnb.rooms : ~2 rows (environ)
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`id`, `country`, `city`, `price`, `type_id`, `size`, `description`, `berth`, `user_id`, `img`) VALUES
	(84, 'France', 'St laurent de la salanque', 44, 2, 35, 'jfgkjfdhglskdjfg', 4, 6, '$2y$10$G2lX6SgSxszpbsaVGJ4N9u5L3rK5s2uJszCzDr2mFAnUXrQs9HG6G.jpg'),
	(85, 'France', 'St laurent de la salanque', 69, 2, 88, 'hjgkghjkghjkghjk', 4, 6, '$2y$10$zlNuzLcCWlHG9N£vARMfHObiZRIIZaOA0mUd9uGlqJ.IkTV8oRLT..png'),
	(86, 'France', 'St laurent de la salanque', 50, 2, 88, 'kgjlfjklfmgjlkmhdfgjkhmjkdfnkljmfghjklmghfd', 1, 6, '$2y$10$FZd2E96cmj3tBhInI1uiWOqzVEPiN6bfe1£NtASNq3u.AkS9QcMgq.jpeg');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;

-- Export de données de la table airbnb.type : ~2 rows (environ)
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` (`id`, `label`) VALUES
	(1, 'Whole housing'),
	(2, 'Private room'),
	(3, 'Shared room');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;

-- Export de données de la table airbnb.users : ~1 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `role_id`) VALUES
	(6, 'admin', 'a703e34c50d96bae4940912d8c8ac5ab36880000625dc9e818a9c9e1703b9c97', 1),
	(7, 'nicolas', 'a703e34c50d96bae4940912d8c8ac5ab36880000625dc9e818a9c9e1703b9c97', 2),
	(8, 'root', 'a703e34c50d96bae4940912d8c8ac5ab36880000625dc9e818a9c9e1703b9c97', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
