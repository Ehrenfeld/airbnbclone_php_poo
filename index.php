<?php
require_once 'app/flight/Flight.php';
require_once 'autoload.php';
require_once  'CONFIG.php';

session_start();

$authController = new AuthController();

Flight::route('/', [$authController, 'home']);

Flight::route('/authentication', [$authController, 'form']);

Flight::route('/disconnection', [$authController, 'disconnection']);

Flight::route('/signin', [$authController, 'signin']);
Flight::route('/login', [$authController, 'login']);

$rentalController = new RentalController();

if ( Auth::isLogged() && Auth::user()->hasRole( Role::STANDARD ) ) {

    Flight::route('/my_booked', [$rentalController, 'myBooked']);

    Flight::route('/booking/@id', [$rentalController, 'bookingById']);
}

if ( Auth::isLogged() && Auth::user()->hasRole( Role::ADVERTISER ) ) {

    Flight::route('/add_rental', [$rentalController, 'addRental']);

    Flight::route('/add/rental', [$rentalController, 'addLocation']);

    Flight::route('/all_my_rental', [$rentalController, 'allMyRentalController'] );




    Flight::route('/rental_booked', [$rentalController, 'rentalBooked']);
}


Flight::start();



