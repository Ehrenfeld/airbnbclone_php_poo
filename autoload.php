<?php 
function shopAutoload( $classe ) {

    $folders = [
        'app/classes',
        'app/classes/models',
        'app/controllers'
    ];

    foreach ( $folders as $folder ) {
        
        $file = $folder . '/' . $classe . '.php';

        if( file_exists( $file ) ) {
            require_once $file;
            return true;
        }

    }

}

spl_autoload_register( 'shopAutoload' );