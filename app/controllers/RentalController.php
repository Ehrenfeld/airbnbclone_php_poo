<?php
/**
 * Created by PhpStorm.
 * User: stage
 * Date: 2019-01-28
 * Time: 11:50
 */

class RentalController extends Controller
{

    public function addRental() {

        $equipements = Equipement::findAll();


        $this->render('add_rental', compact('equipements'));
    }

    public function addLocation() {

        $room = new Room($this->fields);

       if ($this->fields) {

            if(    $_FILES["file"]['type'] != "image/jpg"
                && $_FILES["file"]['type'] != "image/png"
                && $_FILES["file"]['type'] != "image/jpeg"
                && $_FILES["file"]['type'] != "image/gif" ) {

                $this->addError('Sorry, only JPG, JPEG, PNG & GIF files are allowed.');

                $this->redirect('/add_rental');

            }

            if ($_FILES["file"]["size"] > 500000) {

                $this->addError('Sorry, your file is too large.');

                $this->redirect('/add_rental');

            }

            else {

                $tmp_name = $_FILES[ 'file' ]['tmp_name'];
                $name = $_FILES['file']['name'];

                $nmbr1 = strval(rand(0, 1000000000));
                $nmbr2 = strval(rand(0, 1000000));
                $nmbr3 = strval(rand(0, 1000000));


                $temp = explode( '.', $_FILES[ 'file' ][ 'name' ] );

                $name = $name . $nmbr1 . $nmbr2 . $nmbr3;

                $newFileName = password_hash( $name, PASSWORD_BCRYPT);

                $newFileName =  $newFileName . '.' . $temp[1];

                $newFileName = str_replace('/', '£', $newFileName);

                $_FILES['file']['name'] = $newFileName;


                move_uploaded_file( $tmp_name,'public/assets/images/' . $newFileName);


                if ($room->create())
                {

                    $room->linkEquipement();

                    $this->redirect('/');
                }
            }
        }

        else {

            $this->addError('Field(s) are missing...');

            $this->redirect('/add_rental');
        }


    }
    
    public function allMyRentalController() {
        
        $room = new Room();
        
        $rooms = $room->allMyRental($_SESSION['user']->getId());

        $titles = 'My Booking: ';

        $this->render('my_rental', compact('rooms', 'titles'));
    }

    public function bookingById(int $id) {

        $room = new Room;
        

        if($room->bookingById($id)) {

            $this->redirect('/my_booked');
        }

    }

    public function myBooked() {

        $room = new Room();

        $rooms = $room->myBooked();

        $this->render('my_booking', compact('rooms'));
    }

    public function rentalBooked() {

        $room = new Room;

        $rooms = $room->findMyBooking();

        $titles = 'My rooms booked: ';

        $this->render('my_rental', compact('rooms', 'titles'));
    }

}