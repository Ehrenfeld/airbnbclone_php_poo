<?php
/**
 * Created by PhpStorm.
 * User: stage
 * Date: 2019-01-18
 * Time: 09:38
 */

class AuthController extends Controller
{

    function form() {

        $this->render('authentication' );
    }

    function home() {

        $rooms = Room::findAll();

        
        $this->render('home', compact('rooms'));
    }

    function login() {

        if ( $this->checkFields(['username', 'password'])) {

            $auth = new Auth( $this->fields['username'], $this->fields['password'] );

            if ($user = $auth->login()) {

                $_SESSION['user'] = $user;

                $this->redirect('/');

            }
            else $this->addError('Login ou mdp incorrect !');

        }
        else $this->addError('Il manque des champs');

        $this->redirect('/authentication');

    }

    public function signin() {

        if($this->checkFields(['username', 'password', 'password_check', 'role'])) {

            $auth = new Auth(

                $this->fields['username'],
                $this->fields['password'],
                $this->fields['password_check'],
                $this->fields['role']
            );

            if($auth->userExist())
                $this->addError('L\'utilisateur existe déja');

            else if ( !$auth->checkPasswords())
                $this->addError('Les mdp ne correspondent pas');

            else if ($user = $auth->signin())
                $this->redirect('/');
            else
                $this->addError('Une erreur est survenue');
        }

        else $this->addError('Il manque des champs');

        $this->redirect('/authentication');
    }

    public function disconnection() {

        unset($_SESSION['user']);

        $this->redirect('/authentication');
    }
}