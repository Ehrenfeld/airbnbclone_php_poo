<?php

abstract class Controller
{

    protected $title = APP_NAME;
    protected $errors = [];
    protected $fields = [];


    public function __construct()
    {
        if (!empty($_SESSION['errors'])) {

            $this->errors = $_SESSION['errors'];

        }

        $this->fields = $_POST;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    protected function checkFields(array $params): bool {

        foreach ($params as $param ) {

            if ( empty( $this->fields[ $param ])) {
                return false;
            }
        }
        return true;
    }


    protected function addError(string $error ) {

        $this->errors[] = $error;
        $_SESSION['errors'] = $this->errors;
    }


    protected function getErrors(): array
    {
        $_SESSION['errors'] = $this->errors;
        return $this->errors;

    }

    //==============//

    public function getTitle(): string
    {
        return $this->title;
    }

    //==============//

    protected function render(string $view, array $vars = [] ) {

        $vars['title'] = $this->title;
        $vars['errors'] = $this->errors;

        extract( $vars );

        include_once 'public/views/partials/header.php';
        include_once 'public/views/partials/nav.php';
        include_once  "public/views/{$view}.php";
        include_once 'public/views/partials/footer.php';


        unset($_SESSION['errors']);

        die;
    }

    protected function notFound() {

        Flight::notFound();
        die;
    }

    protected function redirect(string $route) {

        Flight::redirect($route);
        die;
    }
}