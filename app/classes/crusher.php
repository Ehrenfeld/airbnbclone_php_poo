<?php

define( 'SALT', '}BX:c]"1+:62LmXvSl!TT][Y>Ppu6[?Y%%@bWkw8*1W0oTKSd(;%[QYKdIIk:A}' );
define( 'PEPPER', '!E^W@W!A_skrtJ5dlWw),>h%KT;Q{<@3+"ZUnlGVYQqZ!UH%qJ/bFzmw/B2lkkn' );
define( 'SUGAR', '(C#9A^HZDeH{S)o<!:86jenTA5vB@n@!3UTVHn48NtVoxUJa^=ViG@Cr=B+qad@' );
define( 'SPICE', '1+E4WB!;lV-<4E@zj%=V~JjQC#oc#8ghjeCjkK{F<HTw8{k`>I"wJ=OW%b*K-B>' );

function crusherHashPassword( string $password ): string
{

    $salt_hash = hash( 'sha256', SALT );
    $pepper_hash = hash( 'sha256', PEPPER );
    $sugar_hash = hash( 'sha256', SUGAR );
    $spice_hash = hash( 'sha256', SPICE );
    $password_hash1 = hash( 'sha256', $password );

    $hash_string = $salt_hash . $spice_hash . $password_hash1 . $pepper_hash . $sugar_hash;

    $password_hash2 = hash( 'sha256', $hash_string );

    $hash_string_final = $pepper_hash . $salt_hash . $password_hash2 . $spice_hash . $sugar_hash;

    $result = hash( 'sha256', $hash_string_final );

    return $result;
}
