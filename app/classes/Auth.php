<?php
/**
 * Created by PhpStorm.
 * User: stage
 * Date: 2019-01-18
 * Time: 09:53
 */

require_once __DIR__ . '/crusher.php';

class Auth
{

    private $username;
    private $password;
    private $password_check;


    public function __construct($username, $password, $password_check = '', $role_id = 0)
    {
        $this->username = $username;
        $this->password = crusherHashPassword( $password);
        $this->password_check = crusherHashPassword( $password_check);
        $this->role_id = $role_id;
    }

    public function login(): ?User {

        $sql = 'SELECT * FROM users WHERE username=:username AND password=:password';

        $stmt = (new Bdd)->getPdo()->prepare($sql);
        $stmt->execute([

            'username' => $this->username,
            'password' => $this->password
        ]);

        $result = $stmt->fetch();

        if (!$result) return null;

        

        $user =  new User($result);
        $_SESSION['user'] = $user;

        return $user;
    }

    public function checkPasswords(): bool {
        
        return $this->password == $this->password_check;
    }

    public function userExist(): bool {

        $sql = 'SELECT id FROM users WHERE username=:username';
        $stmt = (new Bdd)->getPdo()->prepare( $sql );

        $stmt->execute(['username' => $this->username]);

        return $stmt->rowCount() > 0;
    }

    public function signin(): ?User {

        $user = new User;
        $user
            ->setUsername($this->username)
            ->setPassword($this->password)
            ->setRole_id($this->role_id);

        if($user->create()) {

            $_SESSION['user'] = $user;
            return $user;
        }
        else return null;
    }

    public static function isLogged(): bool {

        return isset($_SESSION['user']);
    }

    public static function user(): ?User {

        return self::isLogged() ? $_SESSION['user'] : null;

    }

}