<?php 
abstract class Model {

    protected $id;

    protected $bdd;


    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function __construct(array $datas = [])
    {
        $this->bdd = new Bdd;
        $this->hydrate($datas);
    }

    private function hydrate(array $datas) {

        foreach ($datas as $key => $data) {

            //On génère dynamiquement les méthode setters
            $setter = 'set' . ucfirst($key);

            // Si la method existe
            if (method_exists($this, $setter)) {

                $this->{$setter}($data);
            }
        }
    }

    private function getInsertSqlVars(): string {

        $str = '';

        foreach ($this->toArray() as $key => $value) {
            $str .= ':' . $key . ', ';
        }

        $str = rtrim($str, ', ');
        
        return $str;
    }

    private function getUpdateSqlVars(): string {

        $str = '';
        foreach ($this->toArray() as $key => $value) {
            $str .= $key . '=:' . $key . ', ';
        }

        $str = rtrim($str, ', ');

        return $str;
    }

    abstract protected function getTable(): string;

    abstract protected function toArray(): array;

    public function find(int $id): bool {

        $this->id = $id;

        $sql = "SELECT * FROM {$this->getTable()} WHERE id=:id";

        $stmt = $this
                     ->bdd
                     ->getPdo()
                     ->prepare($sql);

        $stmt->execute(['id' => $this->id]);

        $results = $stmt->fetch();

        if (!$results) return false;

        $this->hydrate($results);
        
        return true;
    }

    public function create(): bool {

        $vars = $this->getInsertSqlVars();
        $sql = "INSERT INTO {$this->getTable()} VALUES (0, {$vars})";
        $pdo = $this->bdd->getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute( $this->toArray() );

        if ($stmt->rowCount() == 0) return false;

        $this->id = $pdo->lastInsertId();
        
        return true;
    }

    public function update(array $datas = []):bool {

        $this->hydrate($datas);

        $vars = $this->getUpdateSqlVars();
        $sql = "UPDATE {$this->getTable()} SET {$vars} WHERE id=:id";

        $stmt = $this->bdd->getPdo()->prepare($sql);

        $params = $this->toArray();
        $params['id'] = $this->id;



        return $stmt->execute($params);
    }

    public function delete() {

        $sql = "DELETE FROM {$this->getTable()} WHERE id=:id";
        $stmt = $this->bdd->getPdo()->prepare($sql);
        $stmt->execute(['id' => $this->id]);

        return $stmt->rowCount() > 0;
    }

    public static function findAll(): array {

        $classname = get_called_class();
        $ref = new $classname;

        $sql = "SELECT * FROM {$ref->getTable()}";
        $results = $ref
                        ->bdd
                        ->getPdo()
                        ->query( $sql )
                        ->fetchAll();

        $models = [];
        foreach ($results as $result) {

            $model = new $classname( $result );

            $models[] = $model;
        }

        return $models;
    }

}