<?php
/**
 * Created by PhpStorm.
 * User: stage
 * Date: 2019-01-28
 * Time: 12:12
 */

class Room extends Model
{
    private $country;
    private $city;
    private $price;

    private $type_id;

    private $equipement = [];

    private $size;
    private $description;
    private $berth;

    private $img;


    public function getImg()
    {
        return $this->img;
    }

    public function setImg($img): void
    {
        $this->img = $img;
    }

    private $date_debut;
    private $date_fin;


    public function getDateDebut()
    {
        return $this->date_debut;
    }

    public function setDateDebut($date_debut): void
    {
        $this->date_debut = $date_debut;
    }

    public function getDateFin()
    {
        return $this->date_fin;
    }

    public function setDateFin($date_fin): void
    {
        $this->date_fin = $date_fin;
    }

    
    private $user_id;


    public function getBerth()
    {
        return $this->berth;
    }

    public function setBerth($berth): void
    {
        $this->berth = $berth;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country): void
    {
        $this->country = $country;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city): void
    {
        $this->city = $city;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public function getType_Id()
    {
        return $this->type;
    }

    public function setType_Id( $type): void
    {
        $this->type = $type;
    }

    public function getEquipement(): array
    {
        return $this->equipement;
    }

    public function setEquipement(array $equipement): void
    {
        $this->equipement = $equipement;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size): void
    {
        $this->size = $size;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    protected function getTable(): string
    {
        return 'rooms';
    }

    protected function toArray(): array
    {
       return [
           'country' => $this->country,
           'city' => $this->city,
           'price' => $this->price,
           'type_id' => $this->type,
           'size' => $this->size,
           'description' => $this->description,
           'berth' => $this->berth,
           'user_id' => $_SESSION['user']->getId(),
           'img' => $_FILES['file']['name']
       ];
    }

    public function addEquipementById( int $id ) {

        $equipement = new Equipement();

        if ( $equipement->find( $id ) ) {
            $this->equipement[] = $equipement;
        }
    }

    public function linkEquipement(): bool {

        $sql = 'INSERT INTO equipement_room VALUES ';

        foreach ($this->equipement as $category ) {

            $sql .= "({$this->id}, {$category}), ";
        }

        $sql = rtrim($sql, ', ');

        $stmt = $this->bdd->getPdo()->query( $sql );

        return $stmt->rowCount() > 0;

    }

    public function findTypeById( $type ) {

        $sql = 'SELECT label FROM `type` WHERE id = :id';

        $stmt = $this->bdd->getPdo()->prepare($sql);
        $stmt->execute(['id' => $type]);

        $result = $stmt->fetch();


        return $result['label'];
    }

    public function allMyRental(int $user_id): array {

        $room = new Room();

        $sql = "SELECT * FROM rooms WHERE user_id = {$user_id}";

        $results = $room
                        ->bdd
                        ->getPdo()
                        ->query( $sql )
                        ->fetchAll();

        $models = [];
        foreach ($results as $result) {

            $model = new Room( $result );

            $models[] = $model;
        }

        
        return $models;
    }

    public function bookingById(int $id) {

        $sql = "INSERT INTO reservation VALUES ({$id}, {$_SESSION['user']->getId()}, DATE '{$_POST['date_debut']}', DATE '{$_POST['date_fin']}')";
        
        $pdo = $this->bdd->getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() == 0) return false;

        $this->id = $pdo->lastInsertId();

        return true;

    }

    public function myBooked(){

        $sql = "SELECT rooms.*, reservation.date_debut, reservation.date_fin 
                FROM rooms 
                INNER JOIN reservation 
                ON rooms.id = room_id AND reservation.user_id = {$_SESSION['user']->getId()}";

        $ref = new Room();

        $results = $ref
                        ->bdd
                        ->getPdo()
                        ->query( $sql )
                        ->fetchAll();

        $models = [];
        foreach ($results as $result) {

            $model = new Room( $result );

            $models[] = $model;
        }

        return $models;

    }

    public function findDate(int $room_id) {

        $sql = "SELECT * FROM reservation WHERE user_id = {$_SESSION['user']->getId()} AND room_id = {$room_id}";

        $results = $this
                        ->bdd
                        ->getPdo()
                        ->query( $sql )
                        ->fetchAll();

        $models = [];
        foreach ($results as $result) {

            $models[] = $result;
        }
        
        return $models;


    }

    public function findMyBooking() {

        $sql = "SELECT rooms.* FROM rooms 
                INNER JOIN reservation ON rooms.id = room_id 
                INNER JOIN users ON rooms.user_id = users.id 
                WHERE users.id = {$_SESSION['user']->getId()}";

        $ref = new Room;

        $results = $ref
                        ->bdd
                        ->getPdo()
                        ->query( $sql )
                        ->fetchAll();

        $models = [];
        foreach ($results as $result) {

            $model = new Room( $result );

            $models[] = $model;
        }

        return $models;
    }

}
