<?php
/**
 * Created by PhpStorm.
 * User: stage
 * Date: 2019-01-28
 * Time: 13:18
 */

class Equipement extends Model
{
    private $label;


    public function getLabel()
    {
        return $this->label;
    }


    public function setLabel($label): void
    {
        $this->label = $label;
    }

    protected function getTable(): string
    {
        return 'equipements';
    }


    protected function toArray(): array
    {
        return ['label' => $this->label];
    }
}