<?php
/**
 * Created by PhpStorm.
 * User: stage
 * Date: 2019-01-18
 * Time: 09:50
 */

class User extends Model
{

    private $username;
    private $password;
    private $role_id;

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setRole_id($role_id)
    {
        $this->role_id = $role_id;
        return $this;
    }


    public function getRoleId()
    {
        return $this->role_id;
    }


    protected function getTable(): string
    {
       return 'users';
    }

    protected function toArray(): array
    {
        return [
            'username' => $this->username,
            'password' => $this->password,
            'role_id' => $this->role_id
        ];
    }

    public function hasRole(string $role ): bool {

        $sql = 'SELECT role_id FROM users WHERE id=:id';
        $stmt = $this->bdd->getPdo()->prepare($sql);
        $stmt->execute(['id' => $this->id]);
        $result = $stmt->fetch();

        return $role == $result['role_id'];

    }
}