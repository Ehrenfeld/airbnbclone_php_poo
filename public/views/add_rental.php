<div class="container">
    <?php include 'partials/errors.php' ?>

    <h1>Create your ad:</h1>

    <form enctype="multipart/form-data" action="/add/rental" method="POST">

        <label> Country:
            <input type="text" name="country"><br>
        </label>

        <label> City:
            <input type="text" name="city"><br>
        </label>

        <label>Type of housing:
            <select name="type_id">
                <option value="1">Whole housing</option>
                <option value="2">Private room</option>
                <option value="3">Shared room</option>
            </select>
        </label>

        <label> Size:
            <input type="text" name="size"> m²<br>
        </label>

        <label> Number of berth :
            <input type="text" name="berth"><br>
        </label>

        <label> Description:
            <textarea name="description" cols="30" rows="10"></textarea><br>
        </label>

        <h3>Equipements: </h3>
        <?php foreach ($equipements as $equipement ): ?>

            <label>
                <span><?php echo $equipement->getLabel() ?></span>
                <input type="checkbox"
                       value="<?php echo $equipement->getId() ?>"
                       name="equipement[]"

            </label>

        <?php endforeach; ?>

        <hr>
        <label> Price per night:
            <input type="text" name="price"> $<br>
        </label>

        <label>Add picture form your ad:
            <i>Only: png, jpg, jpeg, gif.</i>
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" name="file">
                </div>
            </div>
        </label>

        <input type="submit" value="Add !">
    </form>
</div>