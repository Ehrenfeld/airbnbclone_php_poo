<?php include 'partials/errors.php' ?>

<h1><?php echo $titles ?></h1>
<div class="card text-center">

    <?php foreach ($rooms as $room): ?>
        <div class="card-header alert-success">
          <span class="font-weight-bold"><?php echo $room->getCity() . '</span>' . ' in ' . '<span class="font-weight-bold">' . $room->getCountry() . '</span>' ?>
        </div>
        <div class="card-body">
            <h4 class="card-title"><?php echo $room->getPrice() ?> $ per night</h4>

            <h6 class=""><?php echo $room->getSize() ?> m²</h6>

            <span class="card-title">Type: <?php echo $room->findTypeById($room->getType_Id()) ?></span> <br>

            <span class="card-title">For <?php echo $room->getBerth() ?> people</span>

            <p class="card-text border"><?php echo $room->getDescription() ?></p>

        </div>
    <?php endforeach; ?>
</div>

