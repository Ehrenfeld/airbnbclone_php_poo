<?php include 'partials/errors.php' ?>

<h1>All rentals</h1>
<div class="card text-center">

    <?php foreach ($rooms as $room): ?>
      <div class="card-header alert-success">
          <span class="font-weight-bold"><?php echo $room->getCity() . '</span>' . ' in ' . '<span class="font-weight-bold">' . $room->getCountry() . '</span>' ?>
      </div>
          <div class="card-body">
              <img class="card-img-top" src="<?php echo 'public/assets/images/' . $room->getImg() ?>" alt="Card image cap" style="max-width: 30%; height: auto;">

              <hr>
                <h4 class="card-title"><?php echo $room->getPrice() ?> $ per night</h4>

                <h6 class=""><?php echo $room->getSize() ?> m²</h6>

              <span class="card-title"><span class="font-weight-bold">Type:</span> <?php echo $room->findTypeById($room->getType_Id()) ?></span> <br>

              <span class="card-title"><span class="font-weight-bold">For</span> <?php echo $room->getBerth() ?> <span class="font-weight-bold">people</span></span>

                <p class="card-text border"><?php echo $room->getDescription() ?></p>

                  <?php if ( !Auth::isLogged() ): ?>
                    <a href="/authentication" class="btn btn-outline-secondary">Login for book now !</a>
                  <?php endif; ?>

              <?php if ( Auth::isLogged() && Auth::user()->hasRole( Role::STANDARD ) ): ?>
                  <form action="/booking/<?php echo $room->getId() ?>" method="POST">
                        <input type="date" name="date_debut"> to <input type="date" name="date_fin">
                        <input class="btn btn-outline-success" type="submit" value="Book now !">
                  </form>
              <?php endif; ?>

          </div>
    <?php endforeach; ?>
</div>