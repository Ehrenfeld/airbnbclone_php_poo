<?php include 'partials/errors.php' ?>


<div class="row">
    <div class="col-md-6">

        <h2>Login</h2>

        <form action="/login" method="POST">

            <label>
                Username
                <input type="text" name="username">
            </label>

            <label>
                Password
                <input type="password" name="password">
            </label>

            <input type="submit" value="Login">

        </form>
    </div>

    <hr>

    <div class="col-md-6">

        <h2>Signin</h2>

        <form action="/signin" method="POST">

            <label>
                I want to:
                <select name="role">
                    <option value="1">Rent my home.</option>
                    <option value="2">Rent a home.</option>
                </select>
            </label>

            <label>
                Username
                <input type="text" name="username">
            </label>

            <label>
                Password
                <input type="password" name="password">
            </label>

            <label>
                Confirm Password
                <input type="password" name="password_check">
            </label>

            <input type="submit" value="Signin">

        </form>
    </div>
</div>

