<?php include 'partials/errors.php' ?>

<h1>My booking</h1>
<div class="card text-center">

    <?php foreach ($rooms as $room): ?>

        <div class="card-header alert-success">
          <span class="font-weight-bold"><?php echo $room->getCity() . '</span>' . ' in ' . '<span class="font-weight-bold">' . $room->getCountry() . '</span>' ?>
        </div>

        <div class="card-body">

            <?php $models = $room->findDate($room->getId()); ?>

            <?php foreach ($models as $model ): ?>

                <span class="card-title">From <span class="font-weight-bold"><?php echo $model['date_debut']?></span>
                                        to
                                        <span class="font-weight-bold"><?php echo $model['date_fin']; ?></span>
                </span>
                <?php break; ?>
            <?php endforeach; ?>

            <h4 class="card-title"><?php echo $room->getPrice() ?> <span class="font-weight-bold">$ per night</span></h4>

            <h6 class=""><?php echo $room->getSize() ?> m²</h6>

            <span class="card-title"><span class="font-weight-bold">Type:</span> <?php echo $room->findTypeById($room->getType_Id()) ?></span> <br>

            <span class="card-title"><span class="font-weight-bold">For</span> <?php echo $room->getBerth() ?><span class="font-weight-bold"> people</span></span>

            <p class="card-text border"><?php echo $room->getDescription() ?></p>

        </div>
    <?php endforeach; ?>
</div>