<nav>
    <ul>
            <li><a href="/"><i class="fas fa-globe-americas"></i> See all rentals.</a></li>

        <?php if ( Auth::isLogged() && Auth::user()->hasRole( Role::STANDARD ) ): ?>
            <li><a href="/my_booked"><i class="fas fa-calendar-alt"></i> See all my reservations.</a></li>
        <?php endif; ?>

        <?php if ( Auth::isLogged() && Auth::user()->hasRole( Role::ADVERTISER ) ): ?>
        <hr>
            <li><a href="/all_my_rental"><i class="fas fa-eye"></i> See my ad.</a></li>
            <li><a href="/add_rental"><i class="fas fa-plus-square"></i> Add rentals.</a></li>
            <li><a href="/rental_booked"><i class="fas fa-calendar-alt"></i> See my rentals booked.</a></li>
        <?php endif; ?>

        <?php if ( Auth::isLogged() ): ?>
        <hr>

        <a href="/disconnection">
            <button type="button" class="btn btn-outline-danger">
                   <i class="fas fa-user-times"> <span class="font">Disconnection.</span></i>
            </button>
        </a>
        <?php endif; ?>

        <?php if ( !Auth::isLogged() ): ?>
        <hr>
            <li><a href="/authentication"><i class="fas fa-address-card"></i> Login / Signin</a></li>
        <?php endif; ?>
    </ul>

</nav>