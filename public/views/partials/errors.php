<?php if(!empty($errors)): ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach($errors as $error): ?>

            <div class="error"> <?php echo $error ?> </div>

        <?php endforeach; ?>
    </div>
<?php endif; ?>